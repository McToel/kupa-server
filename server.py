from flask import Flask, render_template, send_from_directory
from waitress import serve
import os
app = Flask(__name__)
app.secret_key = os.urandom(12)
# app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0


@app.route('/favicon.ico')
def favicon():
    app.send_static_file('favicon.ico')
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico')


@app.route('/')
def page():
    return render_template('website.html')


if __name__ == "__main__":
    # app.run(host='0.0.0.0', port=5000)
    serve(app, host='0.0.0.0', port=80)
